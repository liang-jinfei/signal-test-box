#! /bin/bash
# 1. 确定包名
package_name=$1
iskeepDir=$2

if [ -z $package_name ]; then
    echo "Please input a version code (for example \"2.2.0\")"
    read package_name
fi

package_name="signal-test-box-v"$package_name

if [ -d $package_name ]; then
    echo "Package directory: $package_name already exists, operation abort!"
    exit -1
fi

# 2. 拷贝exe和demo文件
mkdir $package_name
cp build/signalBox.exe $package_name
cp readme.md $package_name
cp -r matlab_coefficient_demo $package_name
cp -r workspace_demo $package_name

# 3. 拷贝共享库
pushd $package_name
mkdir lib
cp ../build/lib/*.dll lib
windeployqt signalBox.exe
popd

# 4. 打成zip包
zip -r9 $package_name".zip" $package_name

# 5. 清理工作目录
if [ ! $iskeepDir ]; then
    rm -r $package_name
fi
