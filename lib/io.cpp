/*
@file: io.cpp
@author: ZZH
@time: 2022-10-14 15:13:45
@info: io读取数据函数
*/
#include "libFun.h"
#include <QFile>

EXPORT void read_file(pFunCallArg_t pArgs, float* output)
{
    int allCalNum = pArgs->allCalNum;
    char* arg0 = (char*) *(size_t*) pArgs->args[0];

    for (int i = 0; i < allCalNum; i++)
        output[i] = 0;

    QFile file(arg0);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        for (int i = 0;i < allCalNum;i++)
        {
            QString line = file.readLine(64);
            if (line.isEmpty())
                break;
            output[i] = line.toFloat();
        }
    }

    file.close();
}

EXPORT void write_file(pFunCallArg_t pArgs, float* output)
{
    int allCalNum = pArgs->allCalNum;
    char* arg0 = (char*) *(size_t*) pArgs->args[0];
    float* arg1 = static_cast<float*>(pArgs->args[1]);

    memcpy(output, arg1, sizeof(float) * allCalNum);

    QFile file(arg0);
    if (file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate))
    {
        for (int i = 0;i < allCalNum;i++)
        {
            file.write(QString::number(arg1[i]).toStdString().c_str());
            file.write("\n");
        }
    }

    file.close();
}


LibFunction_t funcs[] = {
    LIB_FUNCTION(read_file, 1),
    LIB_FUNCTION(write_file, 2),
    END_OF_LIB
};

register_function_lib(funcs);
