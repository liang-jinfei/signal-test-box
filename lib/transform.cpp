/*
@file: transform.cpp
@author: ZZH
@time: 2022-05-05 11:22:55
@info: 变换函数实现文件
*/
#include "libFun.h"
#include "fft.h"

EXPORT void __dft(pFunCallArg_t pArgs, float* output)
{
    int allCalNum = pArgs->allCalNum;
    float* arg0 = static_cast<float*>(pArgs->args[0]);

    pComplex_t pOut = new Complex_t[allCalNum];

    dft(pOut, arg0, allCalNum);

    memcpy(output, pOut, allCalNum * sizeof(float));
    delete[] pOut;
}

EXPORT void __idft(pFunCallArg_t pArgs, float* output)
{
    int allCalNum = pArgs->allCalNum;
    float* arg0 = static_cast<float*>(pArgs->args[0]);

    pComplex_t pIn = new Complex_t[allCalNum];
    memset(pIn, 0, allCalNum * sizeof(Complex_t));
    memcpy(pIn, arg0, allCalNum * sizeof(float));

    idft(output, pIn, allCalNum);

    delete[] pIn;

    for (size_t i = 0;i < allCalNum;i++)
        output[i] /= allCalNum / 2;
}

LibFunction_t funcs[] = {
    LIB_FUNCTION(__dft, 1, .name = "fft"),
    LIB_FUNCTION(__idft, 1, .name = "ifft"),
    END_OF_LIB
};

register_function_lib(funcs);
