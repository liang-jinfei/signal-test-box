/*
@file: InnerLib.h
@author: ZZH
@time: 2022-11-16 10:15:22
@info: 内置库
*/
#pragma once
#include "libFun.h"

EXPORT void fs(pFunCallArg_t pArgs, float* output);
