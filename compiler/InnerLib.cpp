#include "InnerLib.h"
#include <QDir>

EXPORT void fs(pFunCallArg_t pArgs, float* output)
{
    int allCalNum = pArgs->allCalNum;
    char* arg0 = (char*) *(size_t*) pArgs->args[0];
    memset(output, 0, allCalNum * sizeof(float));

    QDir dir(arg0);
    dir.setFilter(QDir::NoDotAndDotDot | QDir::AllEntries);

    if (not dir.exists())
    {
        UI_INFO("%s is not exist", arg0);
        return;
    }

    LIB_INFO("list all entry of: %s", arg0);
    for (auto& str : dir.entryList())
    {
        LIB_INFO("%s", str.toStdString().c_str());
    }
    LIB_INFO("done");
}
