/*
@file: compiler.h
@author: ZZH
@time: 2022-05-05 17:15:49
@info: 编译组件
*/
#include <QList>
#include <QDir>
#include "compile_common.h"
#include "ast.h"
#include "log.h"
#include "SignalItem.h"
#include "libManager.h"

class Compiler_t
{
private:
    ASTExpress_t* root;
    QString textToParse;
    int recursionCount;
    const int maxRecLim;
    static LibManager_t& libManager;//外部库管理器
    QList<SignalItem*> compile_queue;//编译队列

    Compiler_t(int recLim = 15): maxRecLim(recLim) { COMP_INFO("Init"); this->libManager = LibManager_t::getInst(); }

    ~Compiler_t()
    {
        // if (nullptr != this->root)
        //     delete this->root;

        COMP_INFO("Destroy");
    }

    SignalItem* queuePopFirst(void)
    {
        SignalItem* item = this->compile_queue.first();
        this->compile_queue.pop_front();
        return item;
    }

protected:

    ASTExpress_t* compile(const QString& code);

public:

    friend class SignalItem;

    bool recPush(void)
    {
        if (this->recursionCount < this->maxRecLim)
        {
            this->recursionCount++;
            return true;
        }

        return false;
    }

    inline bool isRecOutOfLim(void) { return this->recursionCount >= this->maxRecLim; }

    bool recPop(void)
    {
        if (this->recursionCount > 0)
        {
            this->recursionCount--;
            return true;
        }

        return false;
    }

    static inline Compiler_t& getInst() { static Compiler_t inst(32); return inst; }

    inline ASTExpress_t* getASTRoot(void) const { return this->root; }
    inline void setASTRoot(ASTExpress_t* const newValue = nullptr) { this->root = newValue; }

    QString& getParseText(void) { return this->textToParse; }

    bool compile(SignalItem* pSignal);

    inline void queuePush(SignalItem* pSignal) { this->compile_queue.push_back(pSignal); }
    inline bool queueIsEmpty(void) const { return this->compile_queue.isEmpty(); }

    static void loadExtLibs(const QString& path);
};

