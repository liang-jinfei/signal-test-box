# Signal Test Box

This is a signal calculate/drawing software based on expression, the main function is sample the signal given by a expression or a function, then execute calculate of the sample sequences, and draw the results into diagram. Using shared library to extend functions is supported.

This software support multiple platforms deployment, this feature is mainly benefit from QT cross-platform API and CMake multiple platforms build ability, you could build applications of different platforms on one host. Currently, the Ubuntu 22.04(based on Win11 WSL2)、Win7/Win10/Win11、Android x86/Android armeabi-v7a(based on Win11 WSA) is tested passed

This software support translates, powered by QT linguist tool. Currently have Chinese simplified translate data(using English in software is default, then use translate data to translate, this is the solution recommended by QT linguist module), if you want to add more language support, you could use QT linguist tool to generate .ts file of this project, such as this file [ui/zh_CN.ts](ui/zh_CN.ts), then translate the item in the .ts file manually, finally release this .ts file to .qm file.

Software will get system locale information at launch time, therefore it can automatically match your system language setting. If your setting is unsupported languages, it will display in English.

If you have questions about using this software、reading the code or building this project, please leave an issue, I will handle it if I see.

- [1. Simple demo](#1-simple-demo)
- [2. Working Principle](#2-working-principle)
- [3. The definition of signal](#3-the-definition-of-signal)

## 1. Simple demo

Input a signal expression, setting the Sample Rate and the Sample Points could see the wave.  
![signal0](./readme_rc/sig1.png)  
![signal1](./readme_rc/sig2.png)

Signal can nested use, to make it easy to use multiple signals to calculate, maximum nest level is 32.  
![signal2](./readme_rc/sig3.png)

Except sin and cos, there are some other functions could be use, such as the software random number function(`rand`) and the hardware random number function(`hrand`).  
![signal3](./readme_rc/sig4.png)  
![signal4](./readme_rc/sig5.png)

Fourier transform, calling `length` function is because of the `fft` function outputs colplex number, the `length` function is to calculate the lenght of complex number, therefore it could calculate the amplitude spectrum.  
![input signal](./readme_rc/mix_sig.png)  
![amplitude spectrum](./readme_rc/fft.png)

The `filters` library has serval filter functions, `lpf` and `hpf` is IIR filters, they are low pass filter and high pass filter. Receive a sample sequence and a stop frequency as arguments, 'fir` is a FIR filter, receive a sample sequence、a filter order and a filter cofficient(can export by Matlab) as arguments.

There are 1 order IIR low pass filter、3 order IIR low pass filter and 32 order FIR low pass filter demo, more information about filter, please check out [10. Filter]()

![OrigSig](./readme_rc/orig_sig.png)  
![IIR_filter1](./readme_rc/iir_lv1.png)  
![IIR_filter2](./readme_rc/iir_lv3.png)  
![FIR_filter](./readme_rc/fir_lv32.png)

You could see, the higher order of the filter has the better effect by analyze the spectrum diagram about the signal which after filter.

## 2. Working Principle

1. A signal could be defined by an expression **f**, at any moment **t**, signal amplitude is **f(t)**
2. If given some moment **t**, the result of the expression **f(t)** is the sample value of signal **f** at the time **t**
3. **t** could calculate by software using Sample Rate, then repeat calculate **N** times, **N** is the Sample Points setting.
4. Software will convert the expression into grammar tree by using the built-in compiler.
5. Before calculating the value of every node, it will calculate the value of sub node, therefore, the value of root node is the value of the whole expression.
6. If a signal reference another signal, the inside signal will calculate first
7. Signals have a maximum nest limitation, to prevent signal reference itself or multiple signal cross reference causes the infinite recursion.

## 3. The definition of signal

In this software, all the signal is defined as an expression, such as `sin(t)` is the sin signal, `sin(100*t) + sin(200*t)` is add two different signals with different frequencies together.
