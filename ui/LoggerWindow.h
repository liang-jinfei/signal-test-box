/*
@file: LoggerWindow.h
@author: ZZH
@time: 2022-11-15 15:03:26
@info: 日志窗口
*/
#pragma once

#include <QtWidgets/QMainWindow>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
#include <QtWidgets/QTextEdit>

#include <qDebug>
#include <QString>
#include <QLocale>
#include <QTranslator>

class LoggerWindow:public QMainWindow
{
    Q_OBJECT
private:
    static QPushButton* pbClose, * pbClear;
    static QTextEdit* pLogDisplayer;
    static QHBoxLayout* phLayout;
    static QVBoxLayout* pvLayout;
    static QWidget* mainWidget;

    void setUp(void);

protected:

public:
    LoggerWindow(QWidget* parent = nullptr):QMainWindow(parent) { this->setUp(); }
    ~LoggerWindow() {}

    static void addLog(const char* fmt, ...);
    // static inline LoggerWindow& getInst(QWidget* parent = nullptr) { static LoggerWindow inst(parent); return inst; }
    
};
