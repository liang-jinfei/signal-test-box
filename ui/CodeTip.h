/*
@file: CodeTip.h
@author: ZZH
@time: 2022-11-04 15:28:24
@info: 代码提示框
*/
#pragma once
#include <QObject>
#include <QListWidget>
#include <QTimer>
#include <QFocusEvent>
#include <QKeyEvent>
#include "log.h"

class CodeTip:public QListWidget
{
    Q_OBJECT
private:

protected:

public:
    explicit CodeTip(QWidget* parent = nullptr);
    ~CodeTip() {}

    void focusOutEvent(QFocusEvent* e) override;
    virtual void keyPressEvent(QKeyEvent* e) override;

signals:
    void selectItemDone(const QString& str) const;

public slots:
    void requestCodeTip(const QPoint& pos, const QString& keyWord);//外部请求代码提示的接口 
};
