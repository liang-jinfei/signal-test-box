/*
@file: SignalEditor.cpp
@author: ZZH
@time: 2022-10-31 13:15:59
@info: 信号编辑器
*/
#include "SignalEditor.h"

void SignalEditor::setUp()
{
    this->setAcceptDrops(true);
    this->pCodeTip = new CodeTip(this);
    this->pCodeTip->setObjectName(QString::fromUtf8("pCodeTip"));

    this->pCodeTipTimer = new QTimer(this);
    this->pCodeTipTimer->setSingleShot(true);
    this->pCodeTipTimer->stop();

    connect(this, &SignalEditor::textChanged, this, [this]() {this->pCodeTipTimer->start(700); this->pCodeTip->hide();});
    connect(this->pCodeTipTimer, &QTimer::timeout, this, &SignalEditor::requestCodeTip);
    connect(this->pCodeTip, &CodeTip::selectItemDone, this, [this](const QString& str) {this->blockSignals(true);this->append(str);this->blockSignals(false);this->setFocus();});
}

void SignalEditor::requestCodeTip(void)
{
    const auto& rect = this->cursorRect();
    this->pCodeTip->requestCodeTip(QPoint(rect.x(), rect.y() + rect.height()), this->toPlainText());
}
