#pragma once
#include <QtCharts/QChart>
#include <QtCharts/QChartView>
#include <QtCharts/QValueAxis>
#include <QtCharts/QLineSeries>
#include <QGraphicsLineItem>
#include <QGraphicsSimpleTextItem>
#include <QLabel>

class ChartView:public QtCharts::QChartView
{
    Q_OBJECT
private:
    void setUp();
    virtual void mouseMoveEvent(QMouseEvent* event) override;
    virtual void mousePressEvent(QMouseEvent* event) override;
    virtual void mouseReleaseEvent(QMouseEvent* event) override;
    virtual void wheelEvent(QWheelEvent* event) override;

protected:
    QGraphicsSimpleTextItem* pPosTip;
    QGraphicsLineItem* xLine, *yLine;
    QPoint prePos;
    bool isPress;
public:
    explicit ChartView(QWidget* parent = nullptr): QChartView(parent) { this->setUp(); }
    explicit ChartView(QtCharts::QChart* chart, QWidget* parent = nullptr): QChartView(chart, parent) { this->setUp(); }
    ~ChartView() {}

    inline void hideCursor() { this->xLine->setVisible(false); this->yLine->setVisible(false); }
};
