/*
@file: SignalEditor.h
@author: ZZH
@time: 2022-10-31 13:16:14
@info: 信号编辑器
*/
#pragma once
#include <QTextEdit>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include "CodeTip.h"
#include "log.h"

class SignalEditor:public QTextEdit
{
    Q_OBJECT
private:
    QTimer* pCodeTipTimer;
    CodeTip* pCodeTip;

    void setUp(void);
protected:

public:
    explicit SignalEditor(QWidget* parent = nullptr): QTextEdit(parent) { this->setUp(); }
    explicit SignalEditor(const QString& text, QWidget* parent = nullptr): QTextEdit(text, parent) { this->setUp(); }
    ~SignalEditor() {}

    void requestCodeTip(void);

    inline void setPlainTextQuiet(const QString& text)
    {
        this->blockSignals(true);
        this->setPlainText(text);
        this->blockSignals(false);
    }

    // void dragEnterEvent(QDragEnterEvent *e) override;
    // void dropEvent(QDropEvent* e) override;
};

