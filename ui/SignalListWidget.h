/*
@file: SignalManager.h
@author: ZZH
@time: 2022-10-18 15:23:14
@info: 信号管理器, 用于导入导出信号列表
*/
#pragma once
#include <QListWidget>
#include <QFile>
#include <QJsonArray>
#include <QJsonObject>
#include <exception>
#include <QKeyEvent>
#include <QTimer>

class SignalListWidget: public QListWidget
{
    Q_OBJECT
private:
    static const char* nameKey;
    static const char* codeKey;
    static const char* isFFTKey;

    virtual bool event(QEvent* event) override;
    QTimer *pTouchTimer;
protected:

public:
    explicit SignalListWidget(QWidget* parent = nullptr);
    ~SignalListWidget() {}

    inline QListWidget* operator << (QJsonArray& arr)
    {
        this->load(arr);
        return this;
    }
    inline QListWidget* operator >> (QJsonArray& arr)
    {
        arr = this->save();
        return this;
    }

    void load(const QJsonArray& arr);
    QJsonArray save();
};
