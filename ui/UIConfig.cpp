/*
@file: UIConfig.cpp
@author: ZZH
@time: 2022-10-28 13:34:27
@info: 初始化各个UI类内的静态常量
*/
#include "mainWindow.h"
#include "ChartView.h"
#include "SignalListWidget.h"

/* MainWindow常量配置 */
//                                    HZ  KHZ    MHZ
const float MainWindow::fsScale[3] = { 1, 1000, 1000000 };
//此处的正则和词法分析器对symbol的要求一致
const QRegExp MainWindow::sigNameRule("[a-zA-Z_][a-zA-Z0-9_]*");
const char* MainWindow::arrayKey = "signal_array";
const char* MainWindow::fsKey = "sample_rate_num";
const char* MainWindow::fsUnitKey = "sample_rate_unit";
const char* MainWindow::calPointsKey = "sample_points";

const int MainWindow::calPointMax = 1024;
const int MainWindow::calPointMin = 8;

/* SignalListWidget常量配置 */
const char* SignalListWidget::nameKey = "name";
const char* SignalListWidget::codeKey = "source_code";
const char* SignalListWidget::isFFTKey = "spectrum_mode";
