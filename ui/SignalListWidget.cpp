/*
@file: SignalManager.cpp
@author: ZZH
@time: 2022-10-18 15:22:38
@info: 信号管理器, 用于导入导出信号列表
*/
#include "SignalListWidget.h"
#include "SignalItem.h"
#include "symTable.h"
#include "log.h"

SignalListWidget::SignalListWidget(QWidget* parent): QListWidget(parent)
{
    this->setAttribute(Qt::WA_AcceptTouchEvents);
    this->pTouchTimer = new QTimer(this);
    this->pTouchTimer->setSingleShot(true);
    this->pTouchTimer->stop();

    connect(this->pTouchTimer, &QTimer::timeout, this, [this]() {emit customContextMenuRequested(this->mapFromGlobal(QCursor::pos()));});
}

void SignalListWidget::load(const QJsonArray& arr)
{
    for (const auto& item : arr)
    {
        QJsonObject sigObj = item.toObject();
        const QString& sigName = sigObj[this->nameKey].toString();
        const QString& sigCode = sigObj[this->codeKey].toString();
        bool isFFTMode = sigObj[this->isFFTKey].toBool();

        if (not SigSymTable.has(sigName))
        {
            auto newItem = new SignalItem(sigName);

            newItem->setSourceCode(sigCode);
            newItem->setFFTMode(isFFTMode);
            this->addItem(newItem);
            SigSymTable.insert(sigName, newItem);//这里的insert必然成功, 因为上面已经检查了是否重名
            UI_INFO("Add signal: %s", sigName.toStdString().c_str());
        }
        else//导入的信号和已有信号冲突, 选择放弃导入的信号, 保留工作区现有信号
        {
            UI_INFO("Skip %s because name conflict", sigName.toStdString().c_str());
        }
    }
}

QJsonArray SignalListWidget::save(void)
{
    QJsonArray arr;
    const int itemCount = this->count();

    for (int i = 0;i < itemCount;i++)
    {
        QJsonObject oneSig;
        auto item = static_cast<SignalItem*>(this->item(i));
        oneSig[this->nameKey] = item->text();
        oneSig[this->codeKey] = item->getSourceCode();
        oneSig[this->isFFTKey] = item->getFFTMode();
        arr.append(oneSig);
    }

    return arr;
}

bool SignalListWidget::event(QEvent* event)
{
    switch (event->type())
    {
        case QEvent::TouchBegin:
            UI_INFO("touch begin");
            // event->accept();
            this->pTouchTimer->start(400);
            QListWidget::event(event);
            return false;

        // case QEvent::TouchCancel:
        case QEvent::TouchUpdate:
        case QEvent::TouchEnd:
            UI_INFO("touch UE");
            // event->accept();
            this->pTouchTimer->stop();
            QListWidget::event(event);
            return false;

        default:
            return QListWidget::event(event);
            break;
    }
}
